
### Statische Analyse von Manifesten
  - https://github.com/zegl/kube-score
  - https://github.com/stackrox/kube-linter
  - https://github.com/bridgecrewio/checkov
  - https://github.com/controlplaneio/kubesec
  - https://github.com/kyverno/kyverno (https://kyverno.io/docs/kyverno-cli/)


![125344371-818b9300-e357-11eb-973a-f1fdfc895821](https://user-images.githubusercontent.com/23377960/125513207-813e060d-87d9-4935-99d8-0d70249a307c.png)

#### Demo / CI ![static-manifest-test](https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/static-manifest-test.yml/badge.svg)
- Workflow-Definition: https://github.com/IG-BvC/cluster-bsi-conformance/blob/main/.github/workflows/static-manifest-test.yml
- Run Workflow: https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/static-manifest-test.yml

### Richtlinien im Cluster erzwingen
  - [Kyverno](https://github.com/kyverno/kyverno) / [Kyverno policies](https://github.com/kyverno/policies)
  - [OPA (Open Policy Agent)](https://www.openpolicyagent.org/) / [Gatekeeper](https://github.com/open-policy-agent/gatekeeper) / [Gatekeeper-Library](https://github.com/open-policy-agent/gatekeeper-library)
  - https://github.com/kubernetes-sigs/security-profiles-operator

#### Demo / CI ![test-policies-in-cluster](https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/test-policies-in-cluster.yml/badge.svg)
 - Workflow-Definition: https://github.com/IG-BvC/cluster-bsi-conformance/blob/main/.github/workflows/test-policies-in-cluster.yml
 - Run Workflow: https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/test-policies-in-cluster.yml

### Explorative Analyse von Clustern / Bewertung der Richtlinienkonformität von Clustern
  - Cluster
    - [Sonobuoy](https://github.com/vmware-tanzu/sonobuoy)
    - [kube-bench](https://github.com/aquasecurity/kube-bench)
    - [Pre-Flight-Checks (replicated)](https://help.replicated.com/docs/kubernetes/packaging-an-application/preflight-checks/) / https://github.com/replicatedhq/troubleshoot
    - [kubeaudit](https://github.com/Shopify/kubeaudit)
    - [kube-hunter](https://github.com/aquasecurity/kube-hunter)
  - Storage: 
    - [kubstr](https://www.cncf.io/blog/2021/04/01/benchmarking-and-evaluating-your-kubernetes-storage-with-kubestr/)


![cluster-exploration-measuring-conformance](https://user-images.githubusercontent.com/23377960/125344386-8bad9180-e357-11eb-80ec-6e8a2fbd8052.png)

#### Demo / CI ![test-cluster-conformance](https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/test-cluster-conformance.yml/badge.svg)
- Workflow-Definition: https://github.com/IG-BvC/cluster-bsi-conformance/blob/main/.github/workflows/test-cluster-conformance.yml 
- Run Workflow: https://github.com/IG-BvC/cluster-bsi-conformance/actions/workflows/test-cluster-conformance.yml
