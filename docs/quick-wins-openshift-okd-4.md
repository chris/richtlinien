## Quick-Wins: Implementierung der Quick-Wins in OpenShift / OKD V4

Hier werden die Quick-Wins aus [quick-wins-poc.md](./quick-wins-poc.md) den entsprechenden implementierten Policies in OpenShift / OKD (Basis ist die Version 4.6+) (OCP/OKD4) gegenübergestellt.

Prinzipiell sind folgende Komponenten in OCP/OKD4 für die prinzipielle Sicherheit und Absicherung der Plattform zuständig:

- SecurityContextConstraints (SCC)
- Standardmäßig sind alle aktuellen Installationen von OCP/OKD4 **multitenant** Installationen.
- Für jedes Projekt, jeden Tenant sollte auf jeden Fall ein OpenShift `Project` (=Namespace) angelegt werden
- Netzwerkisolation der Namespaces kann entweder feingranuliert über NetworkPolicies, EgressNetworkPolicies oder mit NetNamespaces konfiguriert werden.

### Basis-Anforderungen

- SYS.1.6.A5 Separierung der Container
  - Der Betriebssystem-Kernel MUSS über Namespaces (wie Linux cgroups) oder andere geeignete Mechanismen die Container voneinander und von anderen Prozessen auf dem Container-Host trennen. Die Trennung MUSS dabei mindestens Prozess-IDs, Inter-Process-Kommunikation, Benutzer-IDs, Dateisystem und Netz inklusive Hostname umfassen
  - In OCP/OKD4 ist in der Standardkonfiguration (und die sollte man nicht ohne Not ändern) für alle Namespaces / ServiceAccounts / Pods außerhalb der System Namespaces der SCC `restricted`. Dieser SCC verbietet jegliche "Host Features" und verbietet "Host Paths" und "Host Ports".

- SYS.1.6.A6 Verwendung sicherer Images 
  - Nur vertrauenswürdige registries (MUSS)
  - Der Zugriff auf vertrauenswürdige Registries kann innerhalb von OCP/OKD4 auf folgende Methoden beschränkt werden:
    - externer Zugriff via Proxy (und Einschränkung dieser Proxies) gepaart mit einer MachineConfiguration der Nodes, die eine Policy in der Runtime erzwingt
    - Einsatz von Kyverno oder OpenPolicyAgent als Absicherung
    - Einsatz von Singature Verification: entweder GPG Signatures (Red Hat Methode) oder von CoSign und Connaisseur (portablere Methode)
  - Versionsnummern (MUSS) ohne minor-version, um Updates nicht zu behindern (SOLLTE)
  - Es gelten die gleichen Regeln wie beim oberen Punkt. Prinzipiell sind aber in Produktionsumgebungen Digest Deployments vorzuziehen.

### Standard-Anforderungen

- SYS.1.6.A11 Richtlinie für Betrieb und Images
  - Unter anderem dieses Repo
- SYS.1.6.A14 Updates von Containern
  - Beim Start eines Containers SOLLTE der Container-Dienst immer auf die aktuell verfügbare Version des Images prüfen und eine vorhandene neue Version herunterladen. Auf dem Container-Host zwischengespeicherte alte Versionen DÜRFEN dann NICHT gestartet werden.
  - Direkt in OCP/OKD4 nicht erzwingbar: Einsatz von Kyverno oder OpenPolicyAgent notwendig
- SYS.1.6.A15 Unveränderlichkeit der Container
  - readOnlyRootFilesystem
  - Diese Policy kann einfaach via SCC's implementiert werden, ist nur in dem Standard SCC `restricted` nicht aufgedreht.
- SYS.1.6.A21 Container-Ausführung ohne Privilegien
  - Alle Anwendungsdienste in Containern SOLLTEN nur unter einem nicht privilegierten Account gestartet werden. Sie SOLLTEN NICHT über erweiterte Privilegien für die Container-Dienste oder die Cluster-Betriebssoftware verfügen.
  - In der Standard OCP/OKD4 SCC ist der Schalter `RunAsUser` auf `MustRunAsRange` wobei die Range in einer Annotation des Namespaces (typischerweise größer `1000000000` ) erfolgt. Damit ist diese Forderung durch die Plattform direkt abgedeckt. Außerdem erhält auf diese Weise jeder Namespace eine eigene UID GID Range, was die Plattformsicherheit erhöht.
- SYS.1.6.A25
  - Container SOLLTEN jeweils eigene Service-Accounts nutzen, um miteinander und mit den Diensten der Cluster-Betriebssoftware authentifiziert zu kommunizieren, Gruppen von Containern können, wenn sie gleiche Aufgaben haben, einen gemeinsamen Service-Account nutzen. Berechtigungen für die Service-Accounts SOLLTEN nur minimal vergeben sein.
    
    Jeder Dienst, der einen Service-Account nutzt, SOLLTE ein eigenes Token erhalten.
  - Legt man in OCP/OKD4 für ein Projekt ein eigenes `Project`(=namespace) an, so werden innerhalb dieses Namespaces automatisch durch OCP/OKD4 folgende ServiceAccounts angelegt und mit hinreichenden aber auch restriktiven Rechten (allein gültig im aktuellen Namespace) vergeben: `default`, `builder`, `deployer`. Es wird davon ausgegangen, daß innerhalb eines Namespaces eine freie Kommunikation und auch gleiche Security Policies gelten. Dies kann natürlich weiter eingeschränkt werden, allerdings nicht ohne Weiteres erweitert werden.
- SYS.1.6.A26 Accounts der Anwendungsdienste in Containern
  - Die Accounts der Prozesse in den Containern SOLLTEN keine Berechtigungen auf dem Container-Host haben. Wenn dies dennoch notwendig ist, SOLLTEN diese Berechtigungen nur für unbedingt notwendigen Daten gelten.
  - Unter OCP/OKD4 ist die Exekution mit einer generierten UID standardmäßig definiert. Die Fixierung auf eine UID oder das Ausführen im `root` Kontext verlangt ein explizites Setzen von speziellen SCC's für den entsprechenden ServiceAccount (typischerweise in einem isoltierten Namespace).
- SYS.1.6.A27
  - Jedes Image SOLLTE einen Health-Check für den Start und den Betrieb („readiness“ und „liveness“) definieren. Diese Checks SOLLTEN Auskunft über die Verfügbarkeit der Anwendung im Container geben. Sie SOLLTEN fehlschlagen, wenn die Anwendung nicht in der Lage ist, ihre Aufgaben ordnungsgemäß wahrzunehmen. 
 
    Der Container-Dienst oder die Cluster-Betriebssoftware SOLLTEN diese Checks überwachen und Container, bei denen die Checks fehlschlagen, beenden und durch neue Instanzen ersetzen.
  - Direkt in OCP/OKD4 nicht erzwingbar: Einsatz von Kyverno oder OpenPolicyAgent notwendig.

### Anforderungen bei erhöhtem Schutzbedarf

- SYS.1.6.A30 Eigene Trusted Registry für Container
  - Images SOLLTEN nur in einem eigenen Verzeichnis (Registry) bereitgestellt werden. Es SOLLTE durch technische Maßnahmen sichergestellt sein, dass nur Images aus dieser Registry eingesetzt werden.
  - Wie oben (siehe SYS.1.6.A6) schon beschrieben, kann eine entsprechende Policy relativ leicht implementiert werden. Das resultiert auch aus der Tatsache, dass OCP/OKD4 die kompletten Konfigurationen der Nodes (auch das OS und die Container Runtime) zentral managed (MachineConfig und MachineOperator)
- SYS.1.6.A31 Erstellung erweiterter Richtlinien für Container
  - Erweiterte Richtlinien SOLLTEN die Berechtigungen der Container und der betriebenen Anwendungsdienste einschränken. Die Richtlinien SOLLTEN folgende Zugriffe einschränken:
    - Netzverbindungen,
      - Networkseparation wird in OCP/OKD mit folgenden Methoden implementiert / erzwungen:
        - NetworkPolicies
        - EgressNetworkPolicies
        - EgressIP's in Kombination mit Firewall Regeln (extern)
        - namespace-configuration-operator
        - ProjectRequestTemplate
    - Dateisystem-Zugriffe und
    - Kernel-Anfragen (Syscalls)
      - `Allowed Seccomp Profiles` sind in der Standard `restricted` SCC nicht definiert, daher sind die entsprechenden Zugriffe minimiert.
- SYS.1.6.A33 Mikro-Segmentierung von Containern
  - Die Container SOLLTEN nur über die notwendigen Netzports miteinander kommunizieren können. Es SOLLTEN innerhalb der virtuellen Netze Regeln existieren, die alle bis auf die für den Betrieb notwendigen Netzverbindungen unterbinden. Die Regeln SOLLTEN Quelle und Ziel der Verbindungen genau definieren und dafür mindestens die Service-Namen, Meta-Daten („Labels“) oder die Service-Accounts verwenden. Sofern möglich SOLLTEN die Regeln eine zertifikatsbasierte Authentifizierung vorschreiben und nur die in den Zertifikaten hinterlegten Identitäten für die Definition der erlaubten Verbindungen nutzen.
  
    Alle Kriterien, die als Bezeichnung für diese Verbindung dienen, SOLLTEN so abgesichert sein, dass nur berechtige Personen und Verwaltungs-Dienste diese Kriterien setzen dürfen.
  - Mit Hilfe von NetworkPolicies und EgressPolicies kann in OCP/OKD4 eine weitgehend und tiefgehende Security Policy implementiert werden, der sinnvollste und zielführenste Ansatz innerhalb von OCP/OKD4 ist allerdings eine wohhldurchdachte Strategie für die Einteilung in Projects / Namespaces, da man dadurch schon eine saubere Multitenant Umgebung implementieren kann.
